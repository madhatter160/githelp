# Git Commands

Commands I am always looking up plus ones I use frequently for posterity.

## Notes
- The default name of the remote is usually "origin".
- If a branch cannot be merged, the conflicted files in the branch to merge will contain sections that you must delete in order to resolve the conflict.

## Command List

### Check Changes in Local Branch
```
$ git status
```

### Commit Staged Changes

With comment/message
```
$ git commit -m "Your message"
```

### Delete Remote Branch
```
$ git push <remote> --delete <branch>
```

### Force Delete Local Branch
```
$ git branch -D <branch>
```

### Force Overwrite Branch from Remote
```
$ git fetch --all
$ git reset --hard <remote>/<branch>
```

### Get Changes in Remote Branch
```
$ git pull <remote> <branch>
```

### Get Remote Repository
```
$ git clone <repository URL> <directory to clone into>
```

### Get Specific Branch of Remote Repository
Have not cloned repository locally.
```
$ git clone <repository URL> --branch <branch> --single-branch <directory to clone into>
```

Have cloned repository lcoally.
```
$ git checkout <branch>
```

### Merge Local Branches
```
$ git merge <branch to merge>
```

### Push Local Branch to Remote
```
$ git push -u <remote> <branch>
```

### Show Local Branches
```
$ git branch
```

### Show Files In Branch/Commit
The -t option lists subdirectories before descending into them.
Instead of <commit> there can be <branch>.
```
$ git ls-tree -r --name-only <commit>
```

### Stage Changes for Commit

Single file
```
$ git add <name of file>
```

Using wildcard (Example: all files in directory "src")
```
$ git add "src/*"
```

All changes
```
$ git add -A
```

All modified and new, without deleted
```
$ git add .
```

All modified and deleted, without new
```
$ git add -u
```

### Unstage Changes

Specific file
```
$ git reset HEAD <path of file>
```

All files
```
$ git reset HEAD .
```

### Undo Changes
Specific file
```
$ git checkout -- <path of file>
```

All files
```
$ git checkout -- .
```

### Switch Local Branch

Switch to local branch
```
$ git checkout <branch>
```

Create local branch and switch to it
```
$ git checkout -b <new branch>
```

### Remove from Repo

Remove file from repo
```
$ git rm --cached <path to file>
```

Remove directory from repo
```
$ git rm --cached -r <path to directory>
```

### Make change to the previous commit
```
$ git commit --amend
```

### Unstage changes

Unstage a single previously committed file
```
$ git checkout <path of file>
```

Unstage all modified previously committed files
```
$ git checkout -- .
```

Unstage a new file
```
$ git reset <path of file>
```

### Remove items not under version control
Note: Adding ```-f``` will force removal.

Remove files
```
$ git clean
```

Remove files and directories
```
$ git clean -d
```

### Make change to a previous commit
- Save and stash your work so far.
- Look at ```git log``` and copy the first 5 or so characters from the ID of the commit you want to edit onto your clipboard.
- Start the interactive rebase process, pasting in the characters from the ID: ```git rebase -i ID^```.
- Change the word "pick" to "edit" in front of the commit you want to change.
- Save and quit.
- Edit your project files to make the correction.
- Stage the files you want to commit.
- Run ```git commit --all --amend```.
- After you've committed the fixed version, do ```git rebase --continue```.
- Resolve any conflicts by following git's directions.
- Once the rebase is done, re-apply the stash and continue happily with your life.

### Tagging
Tag the latest commit with an annotated tag
```
$ git tag -a <tag name> -m "<tag message>"
```

Push a tag to a remote
```
$ git push <remote> <tag name>
```

Show a tag data and commit
```
$ git show <tag name>
```

List all tags
```
$ git tag
```

List all tags that match a pattern
```
$ git tag -l "<pattern>"
```

### Rename Branch
1. Rename local branch (if you haven't pushed, you're done after this step) ```git branch -m new-name```
2. Delete old branch on the remote and push correctly named local branch ```git push origin :old-name new-name```
3. Rename the upstream branch for the newly named local branch: ```git push origin -u new-name```

# Markdown Help
- [Markdown Tutorial (BitBucket)](https://bitbucket.org/tutorials/markdowndemo)
- [Markdown Cheatsheet (GitHub)](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
- To force a linebreak, put 2 spaces at the end of a line and then press ```Enter```.
- To force 2 linebreaks hit: <Enter> <Space> <Space> <Enter>

# Git Aliases
- [Aliases - Git SCM Wiki](https://git.wiki.kernel.org/index.php/Aliases)